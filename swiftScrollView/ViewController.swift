//
//  ViewController.swift
//  swiftScrollView
//
//  Created by Mohammad Jebelli on ۲۰۱۷/۱/۲۰.
//  Copyright © ۲۰۱۷ cs3260. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scrollview = UIScrollView(frame:self.view.bounds)
        scrollview.backgroundColor = UIColor.blue
        self.view.addSubview(scrollview)
        scrollview.isPagingEnabled = true
        
        scrollview.contentSize = CGSize(width: self.view.frame.size.width * 3, height: self.view.frame.size.height * 3)
        
        let view = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view.backgroundColor = UIColor.yellow
        scrollview.addSubview(view)
        
        
        
        let view2 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height * 2, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view2.backgroundColor = UIColor.red
        scrollview.addSubview(view2)
        
        
        
        let view3 = UIView(frame: CGRect(x: 414, y: 0, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view3.backgroundColor = UIColor.green
        scrollview.addSubview(view3)
        
        
        let view4 = UIView(frame: CGRect(x: 414, y: self.view.frame.size.height, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view4.backgroundColor = UIColor.white
        scrollview.addSubview(view4)
        
        
        
        let view5 = UIView(frame: CGRect(x: 414, y: self.view.frame.size.height * 2, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view5.backgroundColor = UIColor.orange
        scrollview.addSubview(view5)
        
        
        
        let view6 = UIView(frame: CGRect(x: 828, y: 0, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view6.backgroundColor = UIColor.cyan
        scrollview.addSubview(view6)
        
        
        
        let view7 = UIView(frame: CGRect(x: 828, y: self.view.frame.size.height, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view7.backgroundColor = UIColor.black
        scrollview.addSubview(view7)
        
        
        
        let view8 = UIView(frame: CGRect(x: 828, y: self.view.frame.size.height * 2, width: self.view.frame.size.width,  height: self.view.frame.size.height));
        view8.backgroundColor = UIColor.purple
        scrollview.addSubview(view8)
        

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

